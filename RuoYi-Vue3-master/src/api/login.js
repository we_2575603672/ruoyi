import request from '@/utils/request'

// 登录方法
export function login(username, password, code, uuid) {

  const data = {
    "username":username,
    "password":password,
    "code":code,
    "uuid":uuid
  }

  return request({
    url:'/nari-oauth-server/api/v1/oauth/login',
    headers: {
      isToken: false
    },
    contentType: "application/json;charset=utf-8",
    method: 'post',
    data: data
  })
}

// 注册方法
export function register(data) {
  return request({
    url: '/register',
    headers: {
      isToken: false
    },
    method: 'post',
    data: data
  })
}

// 获取用户详细信息
export function getInfo() {
  return request({
    url: '/nari-system-server/api/v1/platform-user/getUserInfo',
    method: 'post'
  })
}

// 退出方法
export function logout() {
  return request({
    url: '/nari-oauth-server/nari-oauth-server/api/v1/oauth/logout',
    method: 'delete'
  })
}

// 获取验证码
export function getCodeImg() {
  return request({
    url: '/nari-oauth-server/api/v1/Captcha/captchaImage',
    headers: {
      isToken: false
    },
    method: 'get',
    timeout: 20000
  })
}