import request from '@/utils/request'

// 获取路由
export const getRouters = () => {
  return request({
    url: '/nari-system-server/api/v1/platform-user/getRouters',
    method: 'get'
  })
}