import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'
const Jti = 'Admin-Jti'
const RefreshToken = "Admin-RefreshToken"
export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function getRefreshToken() {
  return Cookies.get(RefreshToken)
}

export function setRefreshToken(refreshToken) {
  return Cookies.set(RefreshToken, refreshToken)
}

export function removeRefreshToken() {
  return Cookies.remove(RefreshToken)
}

export function getJti() {
  return Cookies.get(Jti)
}

export function setJti(jti) {
  return Cookies.set(Jti, jti)
}

export function removeJti() {
  return Cookies.remove(Jti)
}
